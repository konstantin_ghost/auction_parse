<?
$root_dir = dirname($_SERVER['SCRIPT_FILENAME']);
include_once($root_dir . "/classes/ParserHelper.php");
include_once($root_dir . "/scripts/get_auction.php");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>Получение информации об аукционе</title>

		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/bootstrap-grid.min.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="css/main.css" rel="stylesheet">
	</head>

	<body>
		<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
			<header class="masthead mb-auto">
				<div class="inner">
					<h3 class="masthead-brand">Получение информации об аукционе</h3>
				</div>
			</header>

			<main role="main" class="inner cover">

				<form>
					<div class="form-group">
						<input type="text" name="id" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Номер аукциона" value="<?= !empty($id) ? $id : '' ?>">
						<small id="emailHelp" class="form-text text-muted">введите номер аукциона, например v558151754</small>
					</div>
					<button type="submit" class="btn btn-primary">Получить</button>
				</form>

				<? if (!empty($result)): ?>
					<div class="result">
						<h2>Результат</h2>
						<table class="table">
							<tbody>
								<tr>
									<th scope="row">Название</th>
									<td><?= $result['name'] ?></td>
								</tr>
								<tr>
									<th scope="row">Изображения</th>
									<td>
										<div class="container">
											<? foreach ($result['images'] as $image): ?>
											<div class="row">
												<a href="<?=$image?>"><img src="<?=$image?>"/></a>
											</div>
											<? endforeach; ?>
										</div>
									</td>
								</tr>
								<tr>
									<th scope="row">Текущая цена</th>
									<td><?= $result['price'] ?></td>
								</tr>
								<tr>
									<th scope="row">Имя продавца</th>
									<td><?= $result['seller_name'] ?></td>
								</tr>
								<tr>
									<th scope="row">Рейтинг продавца</th>
									<td><?= $result['seller_rating'] ?></td>
								</tr>
								<tr>
									<th scope="row">Дата и время окончания</th>
									<td><?= $result['date_end']?></td>
								</tr>
								<tr>
									<th scope="row">Описание</th>
									<td>
										<? foreach ($result['description'] as $desc): ?>
											<div class="row">
												<?=$desc['title']?>: <?=$desc['text']?>
											</div>
										<? endforeach; ?>
									</td>
								</tr>
								<tr>
									<th scope="row">Рекомендуемый аукцион</th>
									<td>
										<a href="<?=$result['recommend']?>"><?=$result['recommend']?></a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				<? endif; ?>
			</main>

			<footer class="mastfoot mt-auto">
				<div class="inner">

				</div>
			</footer>
		</div>


		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
	</body>
</html>
