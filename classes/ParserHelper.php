<?php

class ParserHelper
{
	public static function get_html($url, $cookie = FALSE, $login_url = FALSE, $site_url = FALSE, $post_array = FALSE)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array
		(
			'Connection: keep-alive',
			'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
			'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
			'Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.3',
			'Content-type: application/x-7z-compressed',
		));
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.0; ru; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 ( .NET CLR 3.5.30729)");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

		/*if(!empty(ParserConfig::$proxy))
		{
			curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 0);
			curl_setopt($ch, CURLOPT_PROXY, ParserConfig::$proxy);
		}*/

		if($cookie)
		{
			curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
			curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie.txt');
		}

		if($login_url != FALSE && $site_url != FALSE && !empty($post_array))
		{
			$first = TRUE;
			$post = '';
			foreach($post_array as $key => $val)
			{
				if($first)
				{
					$post .= $key.'='.$val;
				}
				else
				{
					$post .= '&'.$key.'='.$val;
				}
				$first = FALSE;
			}
			curl_setopt($ch, CURLOPT_URL, $site_url);
			$html = curl_exec($ch);
			curl_setopt($ch, CURLOPT_URL, $login_url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			$html = curl_exec($ch);
		}
		elseif(!empty($post_array))
		{
			$first = TRUE;
			$post = '';
			foreach($post_array as $key => $val)
			{
				if($first)
				{
					$post .= $key.'='.$val;
				}
				else
				{
					$post .= '&'.$key.'='.$val;
				}
				$first = FALSE;
			}
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			$html = curl_exec($ch);
		}
		else
		{
			curl_setopt($ch, CURLOPT_URL,$url);
			$html = curl_exec($ch);
		}
		
		curl_close($ch);

		return $html;
	}

	public static function get_xpath($html,$utf = FALSE)
	{
		$dom = new DOMDocument();
		$dom->recover = TRUE;
		$dom->strictErrorChecking = FALSE;

		if($utf || strpos(strtoupper($html), 'UTF-8') !== FALSE)
		{
			@$dom->loadHTML('<?xml encoding="UTF-8"?>' . $html);
		}
		else
		{
			@$dom->loadHTML($html);
		}
		$dom->normalizeDocument();
		$xpath = new DOMXPath($dom);
		return $xpath;
	}

	public static function dom_get_node($xpath, $path, $context = NULL, $index = 0)
	{
		$list = $context ? $xpath->query($path, $context) : $xpath->query($path);
		if ( ! $list->length OR ($index !== NULL AND $list->length < $index + 1))
		{
			return NULL;
		}
		if ($index === NULL)
			return $list;
		else
			return $list->item($index);
	}

	public static function dom_get_node_attr($xpath, $path, $attr, $context = NULL, $first = TRUE)
	{
		$list = $context ? $xpath->query($path, $context) : $xpath->query($path);
		if ( ! $list->length)
		{
			return NULL;
		}
		if ($first)
			return $list->item(0)->attributes->getNamedItem($attr)->value;
		else
		{
			$array = array();
			foreach ($list as $item)
				$array[] = $item->attributes->getNamedItem($attr)->value;
			return $array;
		}
	}

	public static function dom_get_node_text($xpath, $path, $context = NULL, $first = TRUE, $index = NULL)
	{
		$list = $context ? $xpath->query($path, $context) : $xpath->query($path);
		if ( ! $list->length OR ($index !== NULL AND $list->length < $index + 1))
		{
			return NULL;
		}
		if ($index !== NULL)
			return $list->item($index)->textContent;
		elseif ($first)
			return $list->item(0)->textContent;
		else
		{
			$array = array();
			foreach ($list as $item)
				$array[] = $item->textContent;
			return $array;
		}
	}
}