<?php
$id = htmlentities($_GET['id']);
if(!empty($id))
{
	$result = array();
	$html = ParserHelper::get_html('https://page.auctions.yahoo.co.jp/jp/auction/'.$id);
	$xpath = ParserHelper::get_xpath($html);
	$result['name'] = trim(ParserHelper::dom_get_node_text($xpath, '//div[@class="l-contentsHead"]//*[@class="ProductTitle__text"]', $el));
	$result['images'] = ParserHelper::dom_get_node_attr($xpath, '//div[@class="l-contentsBody"]//div[@class="ProductImage__footer js-imageGallery-footer"]//img', 'src', $el, FALSE);
	$result['price'] = trim(ParserHelper::dom_get_node_text($xpath, '//div[@class="l-contentsBody"]//*[@class="Price Price--current"]//*[@class="Price__value"]', $el));
	$result['seller_name'] = trim(ParserHelper::dom_get_node_text($xpath, '//div[@class="l-contentsBody"]//*[@class="Seller__name"]', $el));
	$result['seller_rating'] = trim(ParserHelper::dom_get_node_text($xpath, '//div[@class="l-contentsBody"]//*[@class="Seller__ratingSum"]', $el));
	
	$details_title = ParserHelper::dom_get_node_text($xpath, '//div[@class="l-contentsBody"]//*[@class="ProductDetail__title"]', $el, FALSE);
	$details_description = ParserHelper::dom_get_node_text($xpath, '//div[@class="l-contentsBody"]//*[@class="ProductDetail__description"]', $el, FALSE);
	$result['date_end'] = mb_substr(trim($details_description[3]), 1);
	
	foreach ($details_title as $key => $value) 
	{
		$result['description'][$key] = array('title'=>trim($value),'text'=>mb_substr(trim($details_description[$key]), 1));
	}
			
	$result['recommend'] = trim(ParserHelper::dom_get_node_attr($xpath, '//div[@class="l-contentsBody"]//*[@class="Carousel Carousel--patA js-carousel-patA"]//a', 'href', $el));
}